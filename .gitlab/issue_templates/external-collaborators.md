### Description

- *[Describe the issue]
- *[Attach a screenshot or video]
- [Describe how to reproduce the error: only needed if the video or screenshot isn't self explanatory]

(*) Required files

### Labels

- [  ] Accessibility
- [  ] Fix
- [  ] Improvement
- [  ] In Roadmap
- [  ] New
- [  ] Bug
- [  ] Critical
- [  ] Discussion
- [  ] Documentation
- [  ] Enhancement
- [  ] openSUSE
- [  ] Suggestion
- [  ] Support
- [  ] UX-bug

### I confirm that I have

- [  ] Entered all the required information to help maintainers easily verify this issue.
- [  ] Selected a label describing what it is affecting.
