describe.skip('tabs a11y testing', () => {
  before(() => {
    cy.visit('http://localhost:3000/examples/demo-components.html')
    cy.injectAxe()
    cy.wait(600)
  })

  context(
    'Testing all statuses at once for common, shared funtionality',
    () => {
      // Applying a context and run parameters
      it('should comply with wcag21aa', () => {
        cy.checkA11y('[data-component-name="tabs"]', {
          run: 'wcag21aa'
        })
      })
    }
  )

  // TEMPLATE for specific state, you can either clear, delete or reuse it.
  context.skip('default', () => {
    it('wcag21aa test: color, aria-input, forms', () => {
      cy.checkA11y(
        '[data-component-name="tabs"][data-component-status="default"]',
        {
          runOnly: {
            type: 'tag',
            values: ['wcag21aa']
          },
          rules: {
            // Rest of the rules can be found at https://dequeuniversity.com/rules/axe/4.3
            'color-contrast': { enabled: true }
          }
        }
      )
    })
  })
})
