describe('backToTop', () => {
  before(() => {
    cy.visit('/')
    cy.setCookie(
      'annoucement',
      '{%22wasShown%22:true%2C%22campaing%22:%22SUSE%20rebrand%22}'
    )
    cy.scrollTo(0, 900)
  })

  it('Should make back to top button appear', () => {
    cy.get('.back-to-top').should('be.visible')
    cy.get('.back-to-top').click()
    cy.get('.visible > li .submenu-item').should('be.visible')
    cy.get('.back-to-top').should('not.visible')
  })
})
