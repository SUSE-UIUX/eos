$(function () {
  /* Check page classname to decide if we pull or not the data. */

  if ($('.js-a11y-checklist').length) {
    a11yData().then(() => progressBarStateRender())

    // Listen to changes in localstorage to update the a11y-checklist
    window.addEventListener(
      'storage',
      function (e) {
        if (e.key === 'a11y-checklist')
          a11yData().then(() => progressBarStateRender())
      },
      false
    )

    // Listen to the a11y-checkbox change
    $('.js-a11y-interactive-toggle').on('change', function () {
      $('.js-a11y-checklist card').animate('slow', 'easein')

      const state = JSON.parse(localStorage.getItem('a11y-checklist'))
      const updateStatus = JSON.stringify({
        ...state,
        isInteractive: this.checked
      })

      this.checked
        ? $('.js-a11y-interactive-description').show()
        : $('.js-a11y-interactive-description').hide()

      // this will contain a reference to the checkbox
      localStorage.setItem('a11y-checklist', updateStatus)

      return a11yData().then(() => progressBarStateRender())
    })
  }
})

// It will initially fill the section progress bar
const progressBarStateRender = () => {
  const status = JSON.parse(localStorage.getItem('a11y-checklist'))
  status.isInteractive
    ? $('.js-a11y-interactive-description').show()
    : $('.js-a11y-interactive-description').hide()
  return status.data.map((ele) => handleA11YProgressBar(ele.checks, ele.id))
}

/** Fetch the data from the a11y API endpoint */
const a11yData = () =>
  fetch('/api/utils/a11y')
    .then((response) => response.json())
    .then((data) => handleA11YCheckListSection(data))

// Handle the functionality for the a11y list
const handleA11YCheckListSection = (data) => {
  const target = $('.js-a11y-checklist')

  const isInteractive =
    localStorage.getItem('a11y-checklist') === null
      ? false
      : JSON.parse(localStorage.getItem('a11y-checklist')).isInteractive

  // if the data is not saved in localstorage, we'll save it.
  if (localStorage.getItem('a11y-checklist') === null) {
    localStorage.setItem(
      'a11y-checklist',
      JSON.stringify({
        isInteractive: false,
        projectName: '',
        data
      })
    )
  }

  $('.js-a11y-interactive-toggle').prop(
    'checked',
    JSON.parse(localStorage.getItem('a11y-checklist')).isInteractive
  )

  // compare if localstorage and API data are the same
  const localData = JSON.parse(localStorage.getItem('a11y-checklist'))

  // Clear container before re-rendering the HTML
  target.html('')

  // Clearn the isCompleted proprietary from the result since it's the only dynamic value between API and LocalStorage data
  const clearForComparation = (arr) =>
    arr.map((ele) => {
      const modified = ele.checks.map((checkItem) => {
        const { isCompleted, ...rest } = checkItem

        return rest
      })
      return modified
    })

  if (
    JSON.stringify(clearForComparation(localData.data)) ===
    JSON.stringify(clearForComparation(data))
  ) {
    // Use localJSON if data is the same
    target.append(`<div class="accordion a11y-checklist-component">
        ${localData.data
          .map((ele, i) => {
            return a11yComponentAppend(ele, i, isInteractive)
          })
          .join('')}
      </div>`)
  } else {
    // Use new fetched data
    localStorage.setItem(
      'a11y-checklist',
      JSON.stringify({
        isInteractive: false,
        projectName: '',
        data
      })
    )

    target.append(`<div class="accordion a11y-checklist-component">
        ${data
          .map((ele, i) => {
            return a11yComponentAppend(ele, i, isInteractive)
          })
          .join('')}
      </div>`)
  }
}

/** Append a new a11y-checklist element */
const a11yComponentAppend = ({ section, id, checks }, i, isInteractive) => {
  // preview checklist card, only provides description
  const _previewCard = `
    <div class="card">
      <div class="card-header" id="${id}">
        <span class="mb-0 h5">
          <button class="card-header-clickable" tabindex="0" aria-expanded="false" data-toggle="collapse" data-target="#${id}-collapse" aria-controls="${id}-collapse" >
            ${section}
          </button>
        </span>
      </div>

      <div id="${id}-collapse" class="collapse ${
    i === 0 ? 'show' : ''
  }" arial-labelledby="${id}" >
        <div class="card-body">
          <ul>
            ${checks
              .map(
                (ele) =>
                  `<li> ${ele.task}
                  <span> ${ele.description}</span>
                   ${
                     ele.link.length
                       ? `<a target='_blank' href=${ele.link}><i class='eos-icons-outlined eos-18 icon-reset' aria-hidden='true'>open_in_new</i><span class='sr-only'>Opens in a new tab</span> WCAG Link</a>`
                       : ''
                   }
                  </li>`
              )
              .join('')}
          </ul>
        </div>
      </div>
    </div>
  `

  // interactive checklist card, provides description and interactive elements
  const _interactiveCard = `
    <div class="card a11y-intractive-card">
      <div class="card-header" id="${id}">
        <span class="mb-0 h5" data-section-title=${id}>
          <button type="button" class="card-header-clickable" aria-expanded="false" data-toggle="collapse" data-target="#${id}-collapse" aria-controls="${id}-collapse">
          <i class="eos-icons-outlined">task_alt</i>
          ${section}
          </button>
        </span>
      </div>

      <div id="${id}-collapse" class="collapse ${
    i === 0 ? 'show' : ''
  }" arial-labelledby="${id}">
        <div class="card-body">
        <div class='form-group'>
          ${checks
            .map((ele, i) => {
              return `
                  <div class='form-check'>
                    <input onClick="changeSectionItemsState(this)" class='form-check-input eos-checkbox js-a11y-item-checkbox' type='checkbox' data-section-id=${id} data-section-item=${i} id=${`${id}-task-${i}`} ${
                ele.isCompleted ? 'checked' : ''
              }>
                    <label class='form-check-label' for=${`${id}-task-${i}`}>
                    <p class="a11y-checkbox-item-interative ${
                      ele.isCompleted ? 'completed' : ''
                    }" data-item-id=${`${id}-task-${i}`}>${ele.task}
                  <span> ${ele.description}</span>
                   ${
                     ele.link.length
                       ? `<a target='_blank' href=${ele.link}> WCAG Link</a>`
                       : ''
                   }</p>
                    </label>
                </div>
                `
            })
            .join('')}
        </div>
        <div class="a11y-task-completion" data-section-progressbar=${id}>
          <span class="js-a11y-task-progress a11y-task-progress"> </span>
          <span class="js-a11y-task-percentage a11y-task-percentage">0% Completed </span>
        </div>
        </div>
      </div>
       <div class="a11y-task-completion-bottom" data-section-bottom-progressbar=${id}>
          <div class="js-a11y-bottom-progressbar"> </div>
        </div>
    </div>
  `

  // Renders the interactive or preview card based on the isInteractive value
  return isInteractive ? _interactiveCard : _previewCard
}

// Change localStorage based on checkbox status
const changeSectionItemsState = function (ele) {
  // Prepare targeting section and item
  const categoryID = $(ele).attr('data-section-id')
  const itemID = $(ele).attr('data-section-item')

  const itemElement = $(
    `[data-item-id=${`${categoryID}-task-${itemID}`}]`
  ).addClass('completed')

  // Toggle element class for the line-through
  ele.checked
    ? itemElement.addClass('completed')
    : itemElement.removeClass('completed')

  // Find target item in localStorage with given category and item ID
  const a11yLocalStorage = JSON.parse(localStorage.getItem('a11y-checklist'))
  const category = a11yLocalStorage.data.findIndex(
    (ele) => ele.id === categoryID
  )

  // Changes the value of the selected item
  a11yLocalStorage.data[category].checks[itemID].isCompleted = ele.checked

  // Re-writes the localStorage
  localStorage.setItem('a11y-checklist', JSON.stringify(a11yLocalStorage))

  // Re-paint progress bar
  handleA11YProgressBar(a11yLocalStorage.data[category].checks, categoryID)
}

const handleA11YProgressBar = (checks, categoryID) => {
  const completionValue =
    checks.filter((ele) => ele.isCompleted).length === checks.length
      ? 100
      : (checks.filter((ele) => ele.isCompleted).length / checks.length) * 100

  // Changes the section title to green when all taks are completed
  completionValue === 100
    ? $(`[data-section-title=${categoryID}] i`).show()
    : $(`[data-section-title=${categoryID}] i`).hide()

  // Handle the progress bar backround and content of the % text
  $(`[data-section-progressbar=${categoryID}] .js-a11y-task-progress`).css({
    width: `${completionValue}%`
  })
  $(`[data-section-progressbar=${categoryID}] .js-a11y-task-percentage`).text(
    `${Math.floor(completionValue)}% Completed`
  )

  // Fill the border of the element
  $(
    `[ data-section-bottom-progressbar=${categoryID}] .js-a11y-bottom-progressbar`
  ).css({
    width: `${completionValue}%`
  })
}
