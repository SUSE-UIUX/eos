const addAnimatedIconSpecificData = (iconName) => {
  // Start always with Gray as selected
  selectAnimatedIcon('gray', iconName)

  // Change svg and background color on color click for animated icons
  $('.js-icon-color-circle').on('click', function () {
    const colorName = $(this).attr('data-color')

    // Pass icon color and name to change background color and to dowload SVG
    selectAnimatedIcon(colorName, iconName)
  })
}

function selectAnimatedIcon (colorName, iconName) {
  const animatedSVG = `${iconName}-${colorName}.svg`
  // Reset classes in svg container
  $('.js-animated-icons-container')
    .removeClass()
    .addClass('animated-icons-container js-animated-icons-container')
  // Color selection
  $('.js-icon-color-circle').children().addClass('d-none')
  $(`.js-icon-color-${colorName}`).children().removeClass('d-none')
  // Change animated icon and background color of container
  $('.js-animated-icons-container').addClass(
    `animated-icons-container-${colorName}`
  )
  $('.js-animated-eos-icons-svg').attr(
    'src',
    `/images/animated-icons/${animatedSVG}`
  )
  // attached SVG to download button
  $('.js-download-animated-icon')
    .attr('href', `/images/animated-icons/${animatedSVG}`)
    .attr('download', `${animatedSVG}`)
  // change the code example
  $('.js-eos-animated-icons-name').text(`images/${animatedSVG}`)
}
