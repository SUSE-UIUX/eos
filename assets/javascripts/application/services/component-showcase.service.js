const getComponentService = (name, callback) => {
  $.when(
    $.ajax({
      url: `/javascripts/application/models/showcases/${name}.json`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.log(`there was an error retrieving the component showcase data`)
        callback()
      }
    })
  ).then(function (data) {
    callback(data)
  })
}
