/* Copy to clipboard
   ========================================================================== */
$(function () {
  /**
   * Add data-attr to the js-cc class.
   * cc = copy clipboard
   * use it as:
   * .element.js-cc[data-clipboard-text='text to copy to clipboard']
   */

  handleCopyOnClick()
})

const handleCopyOnClick = () => {
  $('.js-cc, .js-cc-hex').on('click', copyToClipboard)

  $('.js-cc, .js-cc-hex').mouseover(function () {
    $(this)
      .attr('data-original-title', 'Copy code to Clipboard')
      .tooltip('dispose')
      .tooltip('show')

    $(this).click(function () {
      let tooltipText = 'Copied HEX!'
      if ($(this).hasClass('js-cc')) {
        tooltipText = 'Value copied!'
      }
      $(this)
        .attr('data-original-title', `${tooltipText}`)
        .tooltip('dispose')
        .tooltip('show')
    })
    $(this).mouseout(function () {
      $(this).tooltip('hide')
    })
  })

  function copyToClipboard () {
    $(this).attr('data-toggle', 'tooltip')
    const value = $(this).data('clipboard-text')

    /* Temporarily create a wrap for the value to copy */
    const $temp = $('<input>')
    $('body').append($temp)
    $temp.val(value).select()
    document.execCommand('copy')
    $temp.remove()
  }
}
