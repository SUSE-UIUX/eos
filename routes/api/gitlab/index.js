const express = require('express')
const router = express.Router()
const fetch = require('node-fetch')
const axios = require('axios')
/* eslint-disable */
router.get('/', (req, res, next) => {
  ;(async () => {
    try {
      const gitlabReq = await fetch(
        'https://gitlab.com/api/v4/projects/3276752/repository/tags'
      )
      const gitRes = await gitlabReq.json()
      res.status(200).send(gitRes)
    } catch (err) {
      console.log(err)
    }
  })()
})

router.get('/repository/', async (req, res, next) => {
  const repositoryUrl = `https://gitlab.com/api/v4/projects/14105386/repository/files/README.md/raw?ref=master`

  const request = await axios.get(repositoryUrl)
  const response = await request.data
  console.log('response: ', response)

  res.status(200).send(response)
})

router.get('/icons/', async (req, res, next) => {
  try {
    const { data } = await axios.get(
      `https://gitlab.com/api/v4/projects/4600360/repository/files/dist%2Fjs%2Feos-icons.json/raw?ref=master`
    )

    res.status(200).send(data.filter((ele) => ele.type === 'static'))
  } catch (error) {
    res.send(error)
  }
})

module.exports = router
