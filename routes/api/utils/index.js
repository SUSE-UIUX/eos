const express = require('express')
const router = express.Router()
const sass = require('node-sass')
const {
  readFilesContentInFolder
} = require('../../../modules/utils/folders-and-files')

/**
 * API Endpoint: /api/utils/scss
 * For the components to render, make sure the index.scss component files has an @import of the variables '../../eos-base/variables/index.scss';
 * If the @import is missing, the server will responde with an error.
 * For an example please check /assets/stylesheets/eos-elements/buttons/index.scss
 * @param components name of the componenent to be copiled
 * @example http://localhost:3000/api/utils/scss/?component=buttons
 */
router.get('/scss', async (req, res) => {
  const { component } = req.query

  sass.render(
    {
      file: `./assets/stylesheets/eos-elements/${component}/index.scss`,
      recursive: true,
      outputStyle: 'compressed'
    },
    function (err, result) {
      if (!err) {
        res.send(result.css.toString())
      } else {
        res.send(err)
      }
    }
  )
})

/**
 * Returns an array of showcase items
 */
router.get('/showcase', async (_, res) => {
  const data = await readFilesContentInFolder(
    '/assets/javascripts/application/models/showcases/'
  )

  res.json(
    data.map((ele) => {
      return ele[0]
    })
  )
})

/**
 * Returns an array of components
 * @example http://localhost:3000/api/utils/a11y
 * @returns {Array}
 */
router.get('/a11y', async (_, res) => {
  const data = await readFilesContentInFolder(
    '/assets/javascripts/application/models/accessibility/'
  )

  return res.json(data)
})

module.exports = router
