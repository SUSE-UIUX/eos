const express = require('express')
const router = express.Router()
const cloudinary = require('cloudinary')
const { zip } = require('zip-a-folder')
const {
  removeDir,
  collectionsList,
  dbDump,
  dbRestore,
  mergeCollections
} = require('../../../modules/mongodb-dump')
const dotEnv = require('dotenv')
dotEnv.config()
// eslint-disable-next-line prefer-destructuring
const MongoClient = require('mongodb').MongoClient
const { getDate } = require('../../../modules/utils/date')

/* Config for Cloudinary API.
 * More about credentials at: https://cloudinary.com/documentation/how_to_integrate_cloudinary#1_create_and_set_up_your_account
 */
cloudinary.config({
  cloud_name: process.env.EOS_CDN_NAME,
  api_key: process.env.EOS_CDN_API_KEY,
  api_secret: process.env.EOS_CDN_API_SEC
})

/* Connect to db based on how we're runing EOS (dev or prod) */
const isDev = process.env.EOS_RUN_PROJECT_AS === 'dev'

const uri = isDev
  ? process.env.EOS_DATABASE_URI_DEV
  : process.env.EOS_DATABASE_URI_PROD

const databaseName = isDev
  ? process.env.EOS_DATABASE_DB_DEV
  : process.env.EOS_DATABASE_DB_PROD

// INFO: Avoid creating multiple connections so we don't excede quota limits https://intercom-help.mongodb.com/en/articles/3069111-connections-in-mongodb-atlas-clusters
const client = new MongoClient(uri + databaseName, { useUnifiedTopology: true })

// Folders to temporally store the dev and prod dumps.
const _devTempBackupFolder = `./temp/dev`
const _prodTempBackupFolder = `./temp/prod`

router.get('/', async (req, res, next) => {
  const backupPath = './backup/dump'

  try {
    /* Takes care of dumping the collections and save them in the backupPath folder */
    // const backupDbCollections = await mongoDump(uri, databaseName, backupPath)
    /* Copress the backupPath folder to an .zip in the dest directory (2nd param of the function) */
    const dump = await dbDump(backupPath, `${uri}${databaseName}`)

    if (dump.status === 200) {
      const fileName = await ZipAFolder.main(backupPath, './backup/')

      /* Push the new generated .zip file to Cloudinary */
      return await cloudinary.v2.uploader
        .upload(`./backup/${fileName}`, {
          resource_type: 'raw',
          folder: 'DB',
          use_filename: true
        })
        .then(() => {
          /* It will remove the backup folder once the zip it's uploade to Cloudinary */
          removeDir('./backup/')
          /* Endoint response with the backupCollection code */
          return res.json({
            message: 'Done uploading the backup'
          })
        })
    }
  } catch (error) {
    console.log('error: ', error)
  }
})

/* Generates a .zip passing a source and dest folder */
class ZipAFolder {
  static async main (src, dest) {
    const { day, month, year } = getDate()
    const fileName = `eos-${
      process.env.EOS_RUN_PROJECT_AS || ''
    }-${day}-${month}-${year}.zip`
    await zip(src, `${dest}/${fileName}`)
    return fileName
  }
}

/**
 * Gets all collection for the current development database and production
 */
router.get('/collections', async (req, res, next) => {
  // Get all collections names from Development
  try {
    const devCollections = await collectionsList({
      client
    })

    // Get all collections names from Production
    const prodCollections = await collectionsList({
      uri: process.env.EOS_DATABASE_URI_PROD,
      dbName: process.env.EOS_DATABASE_DB_PROD
    })

    // Send back the collections list for both envs.
    return res.send({
      devColl: {
        collections: devCollections.filter(
          (ele) => !ele.includes('user_story')
        ),
        name: process.env.EOS_DATABASE_DB_DEV
      },
      prodColl: {
        collections: prodCollections.filter(
          (ele) => !ele.includes('user_story')
        ),
        name: process.env.EOS_DATABASE_DB_PROD
      }
    })
  } catch (error) {
    console.log('error: ', error)
    res.send(error)
  }
})

router.post('/migration', async (req, res, next) => {
  const { dev, prod, newDbName } = req.body

  const { year, month, day } = getDate()
  const newDatabaseName =
    newDbName.length > 0 ? newDbName : `eos-prod-${year}${month}${day}`

  try {
    // Dumps Development
    const dumpDev = await dbDump(_devTempBackupFolder, `${uri}${databaseName}`)

    // Dumps Production
    const dumpProd = await dbDump(
      _prodTempBackupFolder,
      `${process.env.EOS_DATABASE_URI_PROD}${process.env.EOS_DATABASE_DB_PROD}`
    )

    // If both dumps succede, merge both collections in another temp folder.
    if (dumpProd.status === 200 && dumpDev.status === 200) {
      return mergeCollections(
        dev,
        process.env.EOS_DATABASE_DB_DEV,
        prod,
        process.env.EOS_DATABASE_DB_PROD
      ).then(async () => {
        // Once the collections were moved to the temp merge folder, we create the new database to in our production instance with a default name if none is passed from the UI.
        const response = await dbRestore(
          './temp/merged',
          newDatabaseName,
          `${process.env.EOS_DATABASE_URI_PROD}${newDatabaseName}`
        )

        // Removes the temp folder and sends back the response.
        await removeDir('./temp/')
        return res.send(response)
      })
    }
  } catch (error) {
    console.log('error: ', error)
    res.send(error)
  }
})

module.exports = router
