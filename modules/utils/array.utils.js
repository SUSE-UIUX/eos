/**
 * Compare two given arrays
 * @param {array} arr1 first array
 * @param {array} arr2 second array
 * @returns boolean - to display if the two arrays have the same values
 */
const compareSameArrays = (arr1, arr2) => {
  if (!arr1 && !arr2) throw Error('No arrays were provided')
  if (arr1.length === 0 || arr2.length === 0)
    return {
      status: 'error',
      msg: `One of the arrays is an empty array`
    }

  return (
    arr1.filter((item) => arr2.includes(item)).length === arr1.length &&
    arr1.length === arr2.length
  )
}

/**
 * Combines two or more arrays in a unique, single values array.
 * @param {...any} arrays any n number of arrays
 * @returns unique array of values
 */
const uniqueArrayFromArrays = (...arrays) => {
  // Thrown an error if no value was provided
  if (!arrays.length) throw Error('No arrays were provided')

  return [...new Set([].concat(...arrays))]
}

/**
 * Takes an string as KEY and an ARRAY to checked with and returns the list of matches
 * @param {string} key string as the key to scan for.
 * @param {array} array array with values, ex: ['ux', 'a11y', 'ops']
 * @returns {array} array with matches elements
 */
const elementIncludedInArray = (key, array) => {
  if (!key || !array || array.length === 0)
    throw Error('Key or Array was not provided')

  return array
    .map((ele) => {
      if (ele === '') throw Error('Element in array is empty')
      if (`${key.toLocaleLowerCase()}`.startsWith(ele.toLocaleLowerCase()))
        return ele
    })
    .filter((ele) => ele !== undefined)
}

module.exports = {
  compareSameArrays,
  uniqueArrayFromArrays,
  elementIncludedInArray
}
